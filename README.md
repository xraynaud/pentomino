# Pentomino

HTML5 pentomino game.

## How to play
The objective of this game is to tile a rectangle using the 12 different pentominoes. Pentominoes can be rotated by double clicking (multi-touching on tablet) them. After one complete rotation, the pentominoes will be turned upside down. A demo of the program can be found at http://xraynaud.gitlab.io/pentomino.

## How to install
Clone or download as a zip file the repository and open the html file.
