// Pentomino game
// Copyright 2016 Xavier Raynaud.

/*
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/*	Note: uses array.indexOf() which is not supported by IE < version 9.
	Also use destructuring assignments (ie. [foo, bar] = function()) which might not be supported in some browsers.
*/

//function to obtain drawing properties from stylesheet. adapted from https://stackoverflow.com/questions/40978050/set-canvas-strokestyle-color-from-a-css-property
function propertyFromCSSClass(className,property) {
  var tmp = document.createElement("div"), prop;
//  tmp.style.cssText = "position:fixed;left:-100px;top:-100px;width:1px;height:1px";
  tmp.className = className;
  document.body.appendChild(tmp); // required in some browsers
  prop = getComputedStyle(tmp).getPropertyValue(property);
  document.body.removeChild(tmp);
  return prop
}

    // Game related funtions
    //Tiles object definitions
function Tile(id, name, tileType, rotations, reversible, colour) {
    this.id = id;
    this.name = name;
    this.rotations = rotations;
    this.reversible = reversible;
    this.tileType = tileType;
    this.pos = {x: 0, y:0};
    this.colour = colour;
    this.selected = false;
    this.rotated = 0;
    this.snapped = false;
}

    // tile method to reverse tile.
Tile.prototype.reverseTile = function() {
    var newType = Array(this.tileType.length)

    for (i =0; i < this.tileType.length; i++) {
        newType[i] = Array(this.tileType[i].length);
    }
    for (i =0; i <this.tileType.length; i++) {
        for (j =0; j < this.tileType[i].length; j++) {
            newType[i][this.tileType[i].length - 1 -j] = this.tileType[i][j];
        }
    }
    this.tileType = newType;
    this.reversed = 0;

    return true;
}

    //Tile method to rotate tile.
Tile.prototype.rotateTile = function() {
    var newType = Array(this.tileType[0].length)

    for (i =0; i < this.tileType[0].length; i++) {
        newType[i] = Array(this.tileType.length)
    }
    for (i =0; i < this.tileType.length; i++) {
        for (j =0; j < this.tileType[i].length; j++) {
            newType[this.tileType[0].length - 1 - j][i] = this.tileType[i][j];
        }
    }
    this.tileType = newType
    this.rotated = (this.rotated + 1)%this.rotations;
    if (this.rotated==0) {
    	this.reversed=this.reversible;
    }
    return true;
}

    //Tile method to set tile position
Tile.prototype.setPos = function(x,y) {
    this.pos.x = x;
    this.pos.y = y;

    return true;
}

    // Tile method to make tiles snap to the board grid.
Tile.prototype.snap = function(board) {
    var newX;
    var newY;
    var snap = true;
    if (this.pos.x >= board.pos.x-board.dx*0.5 && this.pos.x <= board.pos.x  + board.width*board.dx && this.pos.y >= board.pos.y - 0.5*board.dx && this.pos.y  <= board.pos.y + board.height*board.dx ) {

        newX = this.pos.x - (this.pos.x - board.pos.x)%board.dx;
        newY = this.pos.y - (this.pos.y - board.pos.y)%board.dx;
        if (this.pos.x -newX  > 0.5*board.dx) {
            newX = newX + board.dx;
        }
        if ( this.pos.y-newY > 0.5*board.dx) {
            newY = newY + board.dx;
        }
        var deltax = 0
        var deltay = 0
        for (i = 0; i <this.tileType.length; i++) {
            for (j=0; j< this.tileType[i].length;j++) {
                if (this.tileType[i][j] ==1) {
                    if ((newY + (i+1) * board.dx > board.pos.y + board.dx*board.height) || (newX + (j+1) * board.dx > board.pos.x + board.dx*board.width)) {

                    	snap =	false;
                    } else {
                    	if (board.occupied[(newX - board.pos.x)/board.dx+j][(newY - board.pos.y)/board.dx+i] >=0) {
                        	snap =	false;
                        }
                    }
                }
                deltax = deltax + board.dx
            }
        }

        if (snap) {
            this.pos.x = newX;
            this.pos.y = newY;
            this.snapped = true
            for (i = 0; i <this.tileType.length; i++) {
                for (j=0; j< this.tileType[i].length;j++) {
                    if (this.tileType[i][j] == 1) {
                        board.occupied[(newX - board.pos.x)/board.dx+j][(newY - board.pos.y)/board.dx+i] = this.id
                    }
                }
            }

        }
    }
}


    // Game board
function Board(w,h,dx) {
    var rect = canvas.getBoundingClientRect();

    this.width = w;
    this.height = h;
    this.dx = dx;
    this.pos = {x: margin_left + (rect.width -margin_left - this.dx*this.width)/2, y: margin_top + (rect.height - margin_top -  this.dx*this.height)/2};

    this.occupied = new Array(this.width)
    for (i = 0; i < this.width; i++) {
        this.occupied[i] = new Array(this.height)
    }
    for (i = 0; i < this.width; i++) {
        for (j = 0; j < this.height; j++) {
            this.occupied[i][j] = -1
        }
    }
}


    //Select tile from mouse cursor position
function tileSelection(mouse,tileSet,board) {
    var selected = -1;
    var dragoff = {x: 0, y: 0};
    for (t=0; t<12; t++) {
        myTile = tileSet[t]
        myTile.selected = false;
        var deltax = 0
        var deltay = 0
        if ((mouse.x >= myTile.pos.x &&	mouse.x <= (myTile.pos.x + 5*board.dx)) && ( mouse.y >=	 myTile.pos.y && mouse.y <= (myTile.pos.y + 5*board.dx))) {
            for (i = 0; i <myTile.tileType.length; i++) {

                for (j = 0 ; j	<myTile.tileType[i].length; j++) {

                    if (myTile.tileType[i][j] ==1) {
                        if (mouse.x >=myTile.pos.x+deltax && mouse.y >=myTile.pos.y+deltay && mouse.x <= myTile.pos.x + deltax + board.dx && mouse.y <=myTile.pos.y + deltay + board.dx) {
                            myTile.selected = true;
                            selected = t;
                            dragoff.x = myTile.pos.x - mouse.x;
                            dragoff.y =	myTile.pos.y - mouse.y;
                            stack.splice(stack.indexOf(t),1)
                            stack.push(t);
                        }
                    }
                    deltax = deltax +board.dx
                }
                deltax = 0
                deltay = deltay+board.dx
            }
        }
    }
    if (selected >= 0 && tileSet[selected].snapped == true) {
        tileSet[selected].snapped = false
        for (i = 0; i <tileSet[selected].tileType.length; i++) {
            for (j=0; j< tileSet[selected].tileType[i].length;j++) {
                if (tileSet[selected].tileType[i][j] ==1) {
                    board.occupied[(tileSet[selected].pos.x - board.pos.x)/board.dx+j][(tileSet[selected].pos.y - board.pos.y)/board.dx+i] = -1
                }
                deltax = deltax +board.dx
            }
        }
    }
    return [dragoff,selected];
}

    //Drawing Functions
    //draw one tile.
function drawTile(myTile,board,context) {
    context.strokeStyle=tilesBorders.color;
    context.lineWidth=tilesBorders.width;
    context.translate(myTile.pos.x,myTile.pos.y);

    var deltax = 0
    var deltay = 0

    for (i = 0; i <myTile.tileType.length; i++) {
        for (j=0; j< myTile.tileType[i].length;j++) {
            alpha =1;

            context.fillStyle =myTile.colour;

            if (myTile.tileType[i][j] ==1) {
                context.fillRect(deltax, deltay, board.dx, board.dx);
                context.strokeRect(deltax, deltay, board.dx, board.dx);
            }
            deltax = deltax + board.dx
        }
        deltax = 0
        deltay = deltay+board.dx
    }
    context.translate(-myTile.pos.x,-myTile.pos.y);
    needredraw = false;
}

    // draw Board
function drawBoard(board,context) {
    context.strokeStyle=boardBorders.color;
    context.lineWidth=boardBorders.width;
    context.fillStyle=boardBackground;
    context.rect(board.pos.x, board.pos.y, board.dx*board.width,board.dx*board.height);
    context.fill();
    context.strokeRect(board.pos.x, board.pos.y, board.dx*board.width,board.dx*board.height);
    context.stroke();

    for (i =1; i<board.width; i++) {
	      context.strokeStyle=boardBorders.color;
        context.beginPath();
        context.moveTo(board.pos.x+i*board.dx, board.pos.y)
        context.lineTo(board.pos.x+i*board.dx, board.pos.y+board.height*board.dx)
        context.stroke();
    }

    for (j =1; j<board.height; j++) {
	      context.strokeStyle=boardBorders.color;
        context.beginPath();
        context.moveTo(board.pos.x, board.pos.y+j*board.dx)
        context.lineTo(board.pos.x+board.width*board.dx, board.pos.y+j*board.dx)
        context.stroke();
    }

    for (i =1; i<board.width; i++) {
        for (j =1; j<board.height; j++) {
            if (i > 0 & j > 0) {
  		          context.fillStyle=boardBorders.color;
//document.write(boardBorders.color);
                context.beginPath();
                context.arc(board.pos.x+i*board.dx, board.pos.y+j*board.dx,3,0,2*Math.PI);
                context.fill()
                context.closePath();
            }
        }
    }
};


    // Canvas related functions
function getMousePos(event) {
    var rect = canvas.getBoundingClientRect();
    if (isTouchSupported) {
    	var mx = event.touches[0].clientX - rect.left;
    	var my = event.touches[0].clientY - rect.top;
    } else {
    	var mx = event.clientX - rect.left;
    	var my = event.clientY - rect.top;
    }
    return {x: mx, y: my};
}

    // Events
function doMouseDown(event) {
    mouse = getMousePos(event);
    if (isTouchSupported && event.targetTouches.length > 1) {
    	if (Tiles[selected].reversed != 1) {
            Tiles[selected].rotateTile();
        } else {
            Tiles[selected].reverseTile();
        }
    } else {
    	[dragoff,selected] = tileSelection(mouse, Tiles, board);
    }
    needredraw = true;
}

function doMouseUp(event) {
	if (selected>=0) {
    	if (!isTouchSupported || (isTouchSupported  && event.targetTouches.length == 0)) {
       		Tiles[selected].snap(board) ;
       	 	Tiles[selected].selected = 0;
       	 	selected = -1;
       	 	checkWin(board);
       	 	needredraw = true;
       }
    }
}

function doMoveTile(event) {
    if (selected>=0) {
        mouse = getMousePos(event);
        Tiles[selected].pos.x = mouse.x + dragoff.x;
        Tiles[selected].pos.y = mouse.y + dragoff.y;
        needredraw = true;
    }
}

function doRotateTile(event) {
    mouse = getMousePos(event);
    [dragoff,selected] = tileSelection(mouse, Tiles, board);
    if (selected>=0) {
        if (Tiles[selected].reversed != 1) {
            Tiles[selected].rotateTile();
        } else {
            Tiles[selected].reverseTile();
        }
        needredraw=true;
    }
    selected = -1;
}


function checkWin(board) {
    var count = 0;
    for (i = 0; i < board.width; i++) {
        for (j = 0; j < board.height; j++) {
            if (board.occupied[i][j] >=0) {
                count++
            }
        }
    }
    if (count == board.width*board.height) {
        win = true;
    }
}

function disposeTiles(Tiles,board) {
    var rect = canvas.getBoundingClientRect();
    var ntiles;

    var col = -1;
    var line = -1

    if (board.pos.y > 5*board.dx) {
            //We have space on top and bottom of the board.
    }

    if (board.pos.x-margin_left > 4*board.dx) {
            //We have space on the left and right of the board.
        ntiles = 5;
    } else {
        ntiles = 6;
    }
    gridwidth = (rect.width -1.5*margin_left)/ntiles

    for (i =0; i < 12; i++) {
        col++;
        if (col%ntiles==0) {
            line = line +1
            x = 1.2*margin_left;
            col = 0;
        }
        if (ntiles == 5) {
            switch(line) {
                case 0:
                    y = board.pos.y/2 -board.dx*(Tiles[i].tileType.length)/2 ;
                    break;
                case 1:
                    y = board.pos.y + board.dx*board.height/2 -board.dx*(Tiles[i].tileType.length)/2
                    break;
                case 2:
                    y = board.pos.y + board.dx*board.height +board.pos.y/2  -board.dx*(Tiles[i].tileType.length)/2
                    break;
            }
        } else {
            switch(line) {
                case 0:
                    y = board.pos.y/2 -board.dx*(Tiles[i].tileType.length)/2 ;
                    break;
                case 1:
                    y = board.pos.y + board.dx*board.height +board.pos.y/2  -board.dx*(Tiles[i].tileType.length)/2
                    break;
            }
        }
        Tiles[i].setPos(x + gridwidth/2 -  board.dx*(Tiles[i].tileType[0].length)/2,y);
        if (ntiles == 5 && line == 1 && col == 0 ){
            col = col + 3
            x = x + 3*gridwidth
        }
        x = (x +  gridwidth )
    }
}

function elapsedTime(start) {
    var elapsed = Date.now() - timeStart;
    var hours = Math.floor(elapsed/ 3.6e6);
    var mins = Math.floor((elapsed % 3.6e6)/6e4);
    var secs = Math.floor((elapsed % 6e4) / 1000);

    var txtTime = secs.toString().concat(mins.toString(),':');
    return (mins.toString().concat(':',secs.toString().padStart(2,'0')));
}


    // Canvas
var canvas = document.getElementById('BoardGame');
var parent = document.getElementById("Board");
canvas.width = parent.offsetWidth;
canvas.height = parent.offsetHeight;
var context = canvas.getContext('2d');
var isTouchSupported = 'ontouchstart' in window;
var startEvent = isTouchSupported ? 'touchstart' : 'mousedown';
var moveEvent = isTouchSupported ? 'touchmove' : 'mousemove';
var endEvent = isTouchSupported ? 'touchend' : 'mouseup';

var timeStart = Date.now();
var stopCounter = false;
var win = false;
var timeTaken = "";

canvas.addEventListener(startEvent, doMouseDown, false);
canvas.addEventListener(endEvent, doMouseUp, false);
canvas.addEventListener(moveEvent, doMoveTile, false);
canvas.addEventListener('dblclick', doRotateTile, false);

margin_top = 0.02*canvas.height;
margin_left = 0.15*canvas.width;

    // Game.
    // Games variables
var selected = -1;  // Int indicating which tile is selected, if any.
var dragoff; // needed to have smooth movement of tiles.
var needredraw = true; // Variable to follow to know if redraw is necessary (set to true when a tile is moved)
var stack = [0,1,2,3,4,5,6,7,8,9,10,11]; // z-stack of tiles. Top is last.

    //12 pentomino set
var tileNames = ["Y","V","T","Z","W","F","I","L","X","U","P","N"]
var tileTypes = [ [[1,0],[1,0],[1,1],[1,0]], //Y
                 [[1,0,0],[1,0,0],[1,1,1]], //V
                 [[1,1,1],[0,1,0],[0,1,0]], //T
                 [[1,1,0],[0,1,0],[0,1,1]], //Z
                 [[1,0,0],[1,1,0],[0,1,1]], //W
                 [[0,0,1],[1,1,1],[0,1,0]], //F
                 [[1],[1],[1],[1],[1]], //I
                 [[1,1],[1,0],[1,0],[1,0]], //L
                 [[0,1,0],[1,1,1],[0,1,0]], //X
                 [[1,0,1],[1,1,1]], //U
                 [[1,1],[1,1],[0,1]], //P
                 [[1,0],[1,1],[0,1],[0,1]] //N
                 ];
var rotations = [3,3,3,1,3,3,3,3,0,3,3,3]
var reversible = [1,0,0,1,0,1,0,1,0,0,1,1]
var colours = [
  propertyFromCSSClass("Y","background-color"), propertyFromCSSClass("V","background-color"), propertyFromCSSClass("T","background-color"), propertyFromCSSClass("Z","background-color"), propertyFromCSSClass("W","background-color"), propertyFromCSSClass("F","background-color"), propertyFromCSSClass("I","background-color"), propertyFromCSSClass("L","background-color"), propertyFromCSSClass("X","background-color"), propertyFromCSSClass("U","background-color"), propertyFromCSSClass("P","background-color"), propertyFromCSSClass("N","background-color")
               ];

var boardBackground = propertyFromCSSClass("Board","background-color");
var tmp = propertyFromCSSClass("Board","border");
boardBorders = new Array(3);
boardBorders['width'] = tmp.split(" ")[0].replace("px","");
boardBorders['style'] = tmp.split(" ")[1];
boardBorders['color'] =tmp.replace(boardBorders.width.concat('px',' ',boardBorders.style),'');
var tmp = propertyFromCSSClass("Tiles","border");
tilesBorders = new Array(3);
tilesBorders['width'] = tmp.split(" ")[0].replace("px","");
tilesBorders['style'] = tmp.split(" ")[1];
tilesBorders['color'] =tmp.replace(tilesBorders.width.concat('px',' ',tilesBorders.style),'');
timerStyle = new Array(3);
timerStyle['ffamily'] = propertyFromCSSClass("Timer","font-family");
timerStyle['color'] = propertyFromCSSClass("Timer","color");
timerStyle['fsize'] = propertyFromCSSClass("Timer","font-size");


// Drawing loop.
// http://paulirish.com/2011/requestanimationframe-for-smart-animating/
//http://my.opera.com/emoller/blog/2011/12/20/requestanimationframe-for-smart-er-animating

// requestAnimationFrame polyfill by Erik Möller
// fixes from Paul Irish and Tino Zijdel

(function() {
 var lastTime = 0;
 var vendors = ['ms', 'moz', 'webkit', 'o'];
 for(var x = 0; x < vendors.length && !window.requestAnimationFrame; ++x) {
 window.requestAnimationFrame = window[vendors[x]+'RequestAnimationFrame'];
 window.cancelAnimationFrame = window[vendors[x]+'CancelAnimationFrame']
 || window[vendors[x]+'CancelRequestAnimationFrame'];
 }

 if (!window.requestAnimationFrame)
 window.requestAnimationFrame = function(callback, element) {
 var currTime = new Date().getTime();
 var timeToCall = Math.max(0, 16 - (currTime - lastTime));
 var id = window.setTimeout(function() { callback(currTime + timeToCall); },
                            timeToCall);
 lastTime = currTime + timeToCall;
 return id;
 };

 if (!window.cancelAnimationFrame)
 window.cancelAnimationFrame = function(id) {
 clearTimeout(id);
 };
 }());

function draw() {
    requestAnimationFrame(draw);

    context.clearRect(0, 0, canvas.width, canvas.height);
    context.font = timerStyle.fsize.concat(timerStyle.ffamily,' ');
    context.fillStyle= timerStyle.color;
    if (!stopCounter) {
      context.fillText(elapsedTime(timeStart), Math.round(0.9*canvas.width),Math.round(0.04*canvas.height));
    } else {
      context.fillText(timeTaken, Math.round(0.9*canvas.width),Math.round(0.04*canvas.height));

    }
    drawBoard(board,context);

    for (t =0; t <12; t++) {
        tileid = stack[t];
        drawTile(Tiles[tileid],board, context);
    };

    if (win) {
      if (timeTaken=="") {
        timeTaken = elapsedTime(timeStart);
      }
      alert("You win \nTime taken: "+timeTaken);
      win=false;
      stopCounter = true;

    }

}
//draw();
